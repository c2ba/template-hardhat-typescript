import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@openzeppelin/hardhat-upgrades";
import "@nomiclabs/hardhat-etherscan";

import dotenv from "dotenv";
dotenv.config();

const config: HardhatUserConfig = {
  solidity: "0.8.17",
  networks: {
    frame: {
      url: "http://127.0.0.1:1248",
      timeout: 300000,
    },
    hardhat: {
      chainId: 1337,
    },
  },
  etherscan: {
    apiKey: {
      // Fill environment variables in local .env file
      polygon: process.env.POLYGONSCAN_API_KEY as string, // https://polygonscan.com/
      polygonMumbai: process.env.POLYGONSCAN_API_KEY as string, // https://mumbai.polygonscan.com/
      optimisticEthereum: process.env.OPTIMISM_ETHERSCAN_API_KEY as string, // https://optimistic.etherscan.io/
      optimisticGoerli: process.env.OPTIMISM_ETHERSCAN_API_KEY as string, // https://goerli-optimism.etherscan.io/
    },
  },
};

export default config;
