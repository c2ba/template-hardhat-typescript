# Template hardhat + typescript

This is a template repository for upgradable smart contracts.

## Local setup

Clone the project and install dependencies with:

```bash
pnpm install
```

You can run the tests with:

```bash
pnpm test
```

Take a look at `package.json` for more scripts.
